<?php

class GoldFeedRates_Frontend {

	var $rates;
	var $ounce_to_gram = 31.1;

	function __construct() {
		add_action('init', array($this, 'init_rates'));

		add_shortcode('gold_bid', array($this, 'init_gold_bid_shortcode'));
		add_shortcode('silver_bid', array($this, 'init_silver_bid_shortcode'));
		add_shortcode('platinum_bid', array($this, 'init_platinum_bid_shortcode'));
		add_shortcode('palladium_bid', array($this, 'init_palladium_bid_shortcode'));
	}

	function get_price($atts, $metal = 'gold') {
		$atts = shortcode_atts( array(
			'unit' => 'grams',
			'weight' => 1,
			'purity' => 0.375,
			'payout' => 0.9,
		), $atts );

		// unit="grams" weight="1" purity="0.416" payout="0.90"

		$metal_attribute_name = "{$metal}_bid_usd_toz";

		if (property_exists($this->rates, $metal_attribute_name) !== true) {
			throw new Exception("Metal '{$metal_attribute_name}' doesn't exist");
		}

		$unit_per_ounce = floatval($this->rates->$metal_attribute_name);
		$unit_per_gram = $unit_per_ounce / $this->ounce_to_gram;

		$result_price = ($unit_per_gram * floatval($atts['purity']) * floatval($atts['payout'])) * floatval($atts['weight']);
		$result_price = number_format($result_price, 2, '.', '');

		return $result_price;
	}

	function init_gold_bid_shortcode($atts) {
		return $this->get_price($atts, 'gold');
	}

	function init_silver_bid_shortcode($atts) {
		return $this->get_price($atts, 'silver');
	}

	function init_platinum_bid_shortcode($atts) {
		return $this->get_price($atts, 'platinum');
	}

	function init_palladium_bid_shortcode($atts) {
		return $this->get_price($atts, 'palladium');
	}

	function init_rates(){
		$data = maybe_unserialize(get_option(GOLDFEED_RATES_SETTINGS_PREFIX.'rates'));
		$this->rates = $data;
	}

}

new GoldFeedRates_Frontend();