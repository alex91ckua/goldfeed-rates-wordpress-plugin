<?php

class GoldFeedRates_Admin {

	function __construct() {
		add_action( 'admin_menu', array( $this, 'init_admin_menu' ) );
	}

	function init_admin_menu() {
		add_options_page(
			__('GoldFeed Rates', GOLDFEED_RATES_TD),
			__('GoldFeed Rates', GOLDFEED_RATES_TD),
			'manage_options',
			'goldfeed.php',
			array($this, 'init_options_page')
		);
	}

	function init_options_page() {
		?>

		<div class="wrap"><h2><?php _e('GoldFeed Rates', GOLDFEED_RATES_TD) ?></h2></div>
    <h3><?php _e('Usage', GOLDFEED_RATES_TD) ?></h3>
    <p>
      <?php _e('Example of available shortcodes:', GOLDFEED_RATES_TD) ?><br><br>
      <strong>[gold_bid unit="grams" weight="1" purity="0.375" payout="0.90"]</strong><br>
      <strong>[silver_bid unit="grams" weight="1" purity="0.375" payout="0.90"]</strong><br>
      <strong>[platinum_bid unit="grams" weight="1" purity="0.375" payout="0.90"]</strong><br>
      <strong>[palladium_bid unit="grams" weight="1" purity="0.375" payout="0.90"]</strong><br>
    </p>

		<p><strong><?php _e('Updated at:', GOLDFEED_RATES_TD) ?></strong> <?php echo get_option(GOLDFEED_RATES_SETTINGS_PREFIX.'last_update') ?></p>
		<br>
		<strong><?php _e('Current rates:', GOLDFEED_RATES_TD) ?></strong>
		<pre><?php var_dump(maybe_unserialize(get_option(GOLDFEED_RATES_SETTINGS_PREFIX.'rates', __('No data yet.', GOLDFEED_RATES_TD)))) ?></pre>

		<br>
		<br>
		<p><?php _e('Plugin version', GOLDFEED_RATES_TD) ?>: <?php echo GOLDFEED_RATES_VERSION_NUM; ?></p>

		<?php
	}

}

new GoldFeedRates_Admin();