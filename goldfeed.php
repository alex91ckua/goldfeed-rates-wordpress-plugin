<?php
/*
 * Plugin Name: GoldFeed Rates
 * Description: Get latest rates from GoldFeed by using shortcodes
 * Version: 1.0
 * Author: Alex Masliychuk (alex91ckua)
 * Author URI: http://themeforest.net/user/alex91ckua?ref=alex91ckua
 * Text Domain: gold-feed-rates
 */
	
	if ( !defined('GOLDFEED_RATES_VERSION_NUM'))
		define('GOLDFEED_RATES_VERSION_NUM', '1.0');
	
	if ( !defined( 'GOLDFEED_RATES_PLUGIN_NAME' ) )
		define( 'GOLDFEED_RATES_PLUGIN_NAME', 'GoldFeed Rates' );
	
	if ( !defined( 'GOLDFEED_RATES_TD' ) )
		define( 'GOLDFEED_RATES_TD', 'gold-feed-rates' ); // = text domain (used for translations)
	
	if ( !defined( 'GOLDFEED_RATES_PLUGIN_FILE' ) )
		define( 'GOLDFEED_RATES_PLUGIN_FILE', __FILE__ );
	
	if ( !defined( 'GOLDFEED_RATES_PLUGIN_PATH' ) )
		define( 'GOLDFEED_RATES_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
	
	if ( !defined( 'GOLDFEED_RATES_PLUGIN_URL' ) )
		define( 'GOLDFEED_RATES_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

	if ( !defined( 'GOLDFEED_RATES_DEBUG_MODE' ) )
		define( 'GOLDFEED_RATES_DEBUG_MODE', false );

	if ( !defined( 'GOLDFEED_RATES_SETTINGS_PREFIX' ) )
		define( 'GOLDFEED_RATES_SETTINGS_PREFIX', '_goldfeed_rates_setting_' );

	if ( !defined( 'GOLDFEED_RATES_URL' ) )
		define( 'GOLDFEED_RATES_URL', 'https://gold-feed.com/paid/APIae5b555fcbbd7a6dae7cc5c984f0fd402f1ebd3df50c5df61c/all_metals_json_usd.php' );

	class GoldFeedRates_Init {

		function __construct() {

			add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );

			if ( is_admin() ) {
				add_action( 'plugins_loaded', array( $this, 'admin_init' ), 14 );
			}
			else {
				add_action( 'plugins_loaded', array( $this, 'frontend_init' ), 14 );
			}

			add_filter('cron_schedules', array( $this, 'custom_cron_schedules') );		

			add_action('goldfeed_rates_update_event', array( $this, 'update_gf_rates'));

			register_activation_hook ( GOLDFEED_RATES_PLUGIN_FILE, array( $this, 'activate' ) );
		}

		/**
		 * Set default settings
		 */
		function activate() {

			if (! wp_next_scheduled ( 'goldfeed_rates_update_event' )) {
				wp_schedule_event(time(), '5min', 'goldfeed_rates_update_event');
			}

			if (get_option(GOLDFEED_RATES_SETTINGS_PREFIX.'rates') === false) {
				$this->update_gf_rates();
			}

			if (get_option(GOLDFEED_RATES_SETTINGS_PREFIX.'last_update') === false) {
				update_option(GOLDFEED_RATES_SETTINGS_PREFIX.'last_update', '-');
			}
		}

		function custom_cron_schedules($schedules){
			if(!isset($schedules["5min"])){
				$schedules["5min"] = array(
						'interval' => 5*60,
						'display' => __('Once every 5 minutes'));
			}
			return $schedules;
		}
	
 
		function update_gf_rates() {
			$response = wp_remote_get(GOLDFEED_RATES_URL);

			if ( is_wp_error( $response ) ){
				error_log($response->get_error_message());
			}
			elseif( wp_remote_retrieve_response_code( $response ) === 200 ){
				$body = wp_remote_retrieve_body( $response );
				$serialized_data = maybe_serialize(json_decode($body));
				update_option(GOLDFEED_RATES_SETTINGS_PREFIX.'rates', $serialized_data);
				update_option(GOLDFEED_RATES_SETTINGS_PREFIX.'last_update', date("D M d, Y G:i"));
			}
		}

		/**
		 * Load plugin textdomain.
		 */
		function load_textdomain() {
			load_plugin_textdomain( GOLDFEED_RATES_TD, false, dirname( plugin_basename( GOLDFEED_RATES_PLUGIN_FILE ) ) . '/languages/' );
		}

		/**
		 * Init admin side
		 */
		function admin_init() {
			require_once( GOLDFEED_RATES_PLUGIN_PATH . 'admin/class-admin.php' );
		}

		/**
		 * Init frontend
		 */
		function frontend_init() {
			require_once( GOLDFEED_RATES_PLUGIN_PATH . 'frontend/class-frontend.php' );
		}

	}

new GoldFeedRates_Init();